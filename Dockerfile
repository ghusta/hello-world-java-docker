# See : https://docs.docker.com/develop/develop-images/multistage-build/
FROM maven:3-openjdk-17 as builder
COPY . /code
WORKDIR /code
RUN mvn package -DskipTests -ntp

FROM openjdk:17
COPY --from=builder /code/target/hello-world.jar /
CMD java -cp /hello-world.jar org.example.Hello
