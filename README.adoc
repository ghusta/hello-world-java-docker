= Hello World in Java, tested with Docker
:toc:

== Docker build

Build the Docker image.

[source]
----
docker build . -t java-demo
----

== Docker run

Run the container. The https://docs.docker.com/config/containers/resource_constraints/#limit-a-containers-access-to-memory[maximum amount of memory] allocated to the container is limited to 50 MB.

[source]
----
docker run --rm --memory 50m java-demo
----