package org.example;

public class Hello {

    public static void main(String[] args) {
        System.out.println("Hello World !");
        System.out.println();

        printProperty("JVM Java spec version", System.getProperty("java.vm.specification.version"));
        printProperty("Java version", System.getProperty("java.version"));
        printProperty("JVM version", System.getProperty("java.vm.version"));
        printProperty("Java vendor", System.getProperty("java.vendor"));
        printProperty("Java vendor version", System.getProperty("java.vendor.version"));
        printProperty("Java Bytecode version", System.getProperty("java.class.version"));
        printProperty("OS", System.getProperty("os.name") + " / " + System.getProperty("os.version"));
        printProperty("OS architecture", System.getProperty("os.arch"));
        printProperty("Username", System.getProperty("user.name"));
        printProperty("CPU cores", String.valueOf(Runtime.getRuntime().availableProcessors()));
        printProperty("Memory max (-Xmx)", String.valueOf(Runtime.getRuntime().maxMemory() / (1024 * 1024)) + " MB");
        printProperty("Memory total", String.valueOf(Runtime.getRuntime().totalMemory() / (1024 * 1024)) + " MB");
    }

    private static void printProperty(String desc, String propertyValue) {
        System.out.printf("%-25s : %s\n", desc, (propertyValue == null ? "---" : propertyValue));
    }

}
